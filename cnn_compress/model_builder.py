from typing import List

import torch
import torch.nn.functional as F
from torch import nn, Tensor
from torchvision.models.resnet import resnet18
from yacs.config import CfgNode as CN

Encoding = List[Tensor]


class Encoder(nn.Module):
    def __init__(self, model_cfg: CN):
        super().__init__()
        resnet = resnet18(pretrained=model_cfg.PRETRAINED)

        self._stages = nn.ModuleList()
        self._stages.append(nn.Sequential(resnet.conv1, resnet.bn1, resnet.relu, resnet.maxpool, resnet.layer1))
        self._stages.extend([resnet.layer2, resnet.layer3, resnet.layer4])

        stage_sizes = [64, 128, 256, 512]
        self._encoding_sizes = [model_cfg.EXPANSION_BASE * 2 ** exp for exp in range(len(self._stages))]

        self._encoding_convs = nn.ModuleList()
        for stage_size, enc_size in zip(stage_sizes, self._encoding_sizes):
            self._encoding_convs.append(nn.Sequential(nn.Conv2d(stage_size, enc_size, 1), nn.BatchNorm2d(enc_size)))

    def forward(self, batch: Tensor) -> Encoding:
        encodings = []
        for stage, enc in zip(self._stages, self._encoding_convs):
            batch = stage(batch)
            encodings.append(enc(batch))
        return encodings

    @property
    def encoding_sizes(self):
        return self._encoding_sizes


class RefineBlock(nn.Module):
    def __init__(self, in_channels, out_channels):
        super().__init__()
        self._conv1 = nn.Conv2d(in_channels, out_channels, 1)
        self._bn1 = nn.BatchNorm2d(out_channels)
        self._conv2 = nn.Conv2d(out_channels, out_channels, 3, padding=1)
        self._bn2 = nn.BatchNorm2d(out_channels)

    def forward(self, batch):
        batch = F.relu(self._bn1(self._conv1(batch)))
        return self._bn2(self._conv2(batch))


class Decoder(nn.Module):
    def __init__(self, encoding_sizes: List[int], img_channels: int = 3):
        super().__init__()

        self._blocks = nn.ModuleList()
        self._img_channels = img_channels

        for size in encoding_sizes:
            self._blocks.append(RefineBlock(size + img_channels, img_channels))

        self._upsample = nn.UpsamplingBilinear2d(scale_factor=2)
        self._refine1 = nn.Conv2d(img_channels, img_channels, 5, padding=2)
        self._refine2 = nn.Conv2d(img_channels, img_channels, 5, padding=2)

    def forward(self, encodings: Encoding):
        enc1 = encodings[-1]
        img_shape = torch.tensor(enc1.shape)
        img_shape[1] = self._img_channels
        img = torch.zeros(*img_shape, device=enc1.device, dtype=enc1.dtype)

        for encoding, block in zip(encodings[::-1], self._blocks[::-1]):
            img = img + block(torch.cat([img, encoding], dim=1))
            img = self._upsample(img)

        img = self._upsample(self._refine1(img))

        return self._refine2(img)


class Combined(nn.Module):
    def __init__(self, encoder: nn.Module, decoder: nn.Module):
        super().__init__()
        self._encoder = encoder
        self._decoder = decoder

    def forward(self, batch):
        return self._decoder(self._encoder(batch))

    @property
    def encoder(self):
        return self._encoder

    @property
    def decoder(self):
        return self._decoder


def create_combined_model(cfg: CN):
    encoder = Encoder(cfg)
    decoder = Decoder(encoder.encoding_sizes)
    return Combined(encoder, decoder)
