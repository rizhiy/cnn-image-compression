import argparse
from pathlib import Path
from typing import Optional, Tuple

import cv2
import numpy as np
import torch
from tqdm import tqdm

from cnn_compress.config import transform_cfg, get_default_cfg, load_cfg
from cnn_compress.dataset import CompressDataset
from cnn_compress.utils import create_logger, create_infer_transform, create_infer_detransform, pad_image, load_model


def criterion(orig_img: np.ndarray, compressed_img: np.ndarray) -> float:
    return float(np.mean((orig_img - compressed_img) ** 2))


def test(cfg_path: Optional[Path] = None, cpu=False, max_size=12_000_000) -> Tuple[float, float]:
    """
    Test how well a model is trained
    :param cfg_path: path to configuration
    :param cpu: Whether to use CPU
    :param max_size: Maximum number of values in an image (3*width*height).
        Images larger than that will be scaled down.
    :return: Loss and Average Compression Rate
    """
    GPU = not cpu
    cfg = transform_cfg(get_default_cfg()) if cfg_path is None else load_cfg(cfg_path)

    logger = create_logger(cfg.NAME + ' TEST', Path(cfg.OUTPUT_DIR) / 'test.log')

    transform = create_infer_transform(cfg)
    detransform = create_infer_detransform(cfg)

    logger.info("Creating Dataset")
    dataset = CompressDataset(Path(cfg.DATA.ROOT_DIR), lambda x: x, cfg.DATA.FORMAT, train=False)
    logger.info(f"Dataset Created: {len(dataset)} images")

    model = load_model(cfg)

    if GPU:
        model = model.cuda()

    model.eval()

    total_loss = 0
    compression_rate = 0

    for orig_img in tqdm(dataset, smoothing=0.1):
        orig_size = np.prod(orig_img.shape)
        if np.prod(orig_img.shape) > max_size:
            scale = np.sqrt(max_size / orig_size)
            orig_img = cv2.resize(orig_img, None, None, fx=scale, fy=scale)
            orig_size = np.prod(orig_img.shape)
        padded_img, (height, width) = pad_image(orig_img)
        model_input = transform(padded_img).unsqueeze(0)
        if GPU:
            model_input = model_input.cuda()
        with torch.no_grad():
            encoding = model.encoder(model_input)
            encoding_size = np.sum([np.prod(x.shape) for x in encoding])
            output = model.decoder(encoding)[0]

        if GPU:
            output = output.cpu()
        output = detransform(output)
        reconstructed_img = output[:height, :width, :]

        total_loss += criterion(orig_img, reconstructed_img)

        compression_rate += orig_size / encoding_size

    avg_loss = total_loss / len(dataset)
    avg_compression_rate = compression_rate / len(dataset)

    logger.info(f"Average Loss={avg_loss:.3f}")
    logger.info(f"Average Compression Rate={avg_compression_rate:.1f}")

    return avg_loss, avg_compression_rate


if __name__ == '__main__':
    parser = argparse.ArgumentParser("Train the model")
    parser.add_argument('--cfg', type=Path, help="Path to config file")
    parser.add_argument('--cpu', action='store_true', help="Use CPU")
    parser.add_argument('--max-size', type=int, help="Images larger than this size will be scaled down",
                        default=12_000_000)
    args = parser.parse_args()

    test(args.cfg, args.cpu)
