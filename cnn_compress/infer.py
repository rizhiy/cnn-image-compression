import argparse
import pickle
from pathlib import Path
from typing import Optional

import cv2
import numpy as np
import torch

from cnn_compress.utils import pad_image, load_cfg, \
    load_model, create_infer_transform, create_infer_detransform


def compress(img_path: str, cfg_path: Optional[Path] = None, gpu=False) -> str:
    """
    Compress an image
    :param img_path: path to the image
    :param cfg_path: path to configuration
    :param gpu: Whether to use GPU
    :return: path to encoding file
    """
    cfg = load_cfg(cfg_path)

    model = load_model(cfg).encoder
    transform = create_infer_transform(cfg)

    img = cv2.imread(img_path)
    img, orig_size = pad_image(img)
    batch = transform(img).unsqueeze(0)

    img_size = np.prod(img.shape)

    if gpu:
        model = model.cuda()
        batch = batch.cuda()

    with torch.no_grad():
        out = model(batch)

    if gpu:
        out = [x.cpu() for x in out]

    compression_size = np.sum([np.prod(x.shape) for x in out])

    encoding_path = img_path.rsplit('.', 1)[0] + '.encoding'
    with open(encoding_path, 'wb') as f:
        pickle.dump({"encoding":  out,
                     "orig_size": orig_size}, f)

    print(f"Compression rate: {img_size / compression_size:.1f}")

    return encoding_path


def decompress(encoding_path: str, cfg_path: Optional[Path] = None, gpu=False) -> str:
    """
    Decompress an image from encoding
    :param encoding_path: path to encoding
    :param cfg_path: path to configuration
    :param gpu: Whether to use GPU
    :return: path to uncompressed image
    """
    cfg = load_cfg(cfg_path)

    model = load_model(cfg).decoder
    detransform = create_infer_detransform(cfg)

    with open(encoding_path, 'rb') as f:
        compression = pickle.load(f)

    batch = compression['encoding']
    height, width = compression['orig_size']

    if gpu:
        model = model.cuda()
        batch = [x.cuda() for x in batch]

    with torch.no_grad():
        out = model(batch)[0]

    if gpu:
        out = out.cpu()

    img = detransform(out)
    img = img[:height, :width, :]

    decompression_path = encoding_path.rsplit('.', 1)[0] + '.uncompressed.png'

    cv2.imwrite(decompression_path, img)
    return decompression_path


if __name__ == '__main__':
    parser = argparse.ArgumentParser("Compress-decompress an image")
    parser.add_argument("img_path", type=str)
    parser.add_argument("--cfg", type=Path)
    args = parser.parse_args()

    path = compress(args.img_path, args.cfg)
    decompress(path, args.cfg)
