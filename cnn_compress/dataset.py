import random
from pathlib import Path
from typing import Callable

import cv2
import imagehash
import numpy as np
from PIL import Image
from google_images_download import google_images_download
from torch import Tensor
from torch.utils.data import Dataset
from tqdm import tqdm

nouns = ["ability", "absence", "abuse", "access", "accident", "accommodation", "account", "acid", "act", "action",
         "activity", "addition", "address", "administration", "adult", "advance", "advantage", "advice", "afternoon",
         "age", "agency", "agent", "agreement", "agriculture", "aid", "aim", "air", "aircraft", "alan", "alternative",
         "amount", "analysis", "animal", "answer", "appeal", "appearance", "application", "appointment", "approach",
         "approval", "area", "argument", "arm", "army", "arrival", "art", "article", "artist", "aspect", "assembly",
         "assessment", "assistance", "association", "atmosphere", "attack", "attempt", "attention", "attitude",
         "audience", "author", "authority", "autumn", "average", "award", "awareness", "baby", "back", "background",
         "bag", "balance", "ball", "band", "bank", "bar", "base", "basis", "bath", "battle", "beach", "beauty", "bed",
         "bedroom", "behalf", "behaviour", "belief", "benefit", "bill", "bird", "birth", "bishop", "block", "blood",
         "board", "boat", "bob", "body", "book", "border", "bottle", "bottom", "box", "boy", "brain", "branch", "bread",
         "break", "breakfast", "breath", "bridge", "brother", "brown", "budget", "bus", "bush", "business", "cabinet",
         "call", "campaign", "cancer", "candidate", "capacity", "capital", "captain", "car", "card", "care", "career",
         "case", "cash", "castle", "cat", "cause", "cell", "centre", "century", "chain", "chair", "chairman",
         "challenge", "championship", "chance", "chancellor", "change", "channel", "chapter", "character", "charge",
         "charity", "chest", "chief", "child", "china", "choice", "christ", "church", "city", "claim", "class",
         "clause", "client", "club", "cooperation", "coal", "coast", "code", "coffee", "collection", "college",
         "colour", "combination", "command", "commission", "commitment", "committee", "communication", "community",
         "company", "comparison", "competition", "computer", "concentration", "concept", "concern", "conclusion",
         "condition", "conference", "confidence", "conflict", "congress", "connection", "consequence", "consideration",
         "constitution", "construction", "consumer", "contact", "content", "context", "contract", "contrast",
         "contribution", "control", "convention", "conversation", "copy", "core", "corner", "corporation", "cost",
         "council", "country", "countryside", "county", "couple", "course", "court", "cover", "creation", "credit",
         "crime", "crisis", "criticism", "cross", "crowd", "crown", "culture", "cup", "currency", "curriculum",
         "customer", "dad", "damage", "danger", "date", "daughter", "day", "deal", "death", "debate", "debt", "decade",
         "decision", "decline", "defence", "definition", "degree", "delivery", "demand", "democracy", "department",
         "deputy", "description", "design", "desire", "desk", "detail", "development", "diet", "difference",
         "difficulty", "dinner", "direction", "director", "discipline", "discussion", "disease", "display", "distance",
         "distinction", "distribution", "district", "division", "doctor", "document", "dog", "door", "doubt", "drama",
         "dream", "dress", "drink", "drive", "driver", "drug", "duty", "earth", "east", "economy", "edge", "editor",
         "education", "effect", "efficiency", "effort", "election", "electricity", "element", "emergency", "emphasis",
         "empire", "employment", "end", "energy", "engine", "english", "enterprise", "entry", "environment",
         "equipment", "error", "establishment", "estate", "event", "evidence", "examination", "example", "exchange",
         "executive", "exercise", "exhibition", "existence", "expansion", "expenditure", "experience", "expert",
         "explanation", "expression", "extension", "extent", "eye", "face", "fact", "factor", "factory", "failure",
         "faith", "fall", "family", "farm", "fashion", "father", "favour", "fear", "feature", "field", "fig.", "figure",
         "file", "film", "finance", "fire", "firm", "fish", "flat", "flight", "floor", "flow", "focus", "food", "foot",
         "football", "force", "forest", "form", "formation", "foundation", "framework", "frank", "freedom", "friend",
         "front", "fruit", "fuel", "fun", "function", "fund", "furniture", "future", "gallery", "game", "gap", "garden",
         "gas", "gate", "general", "generation", "gentleman", "girl", "glass", "goal", "god", "gold", "golf",
         "government", "graham", "grant", "grass", "green", "group", "growth", "guide", "gun", "guy", "hair", "half",
         "hall", "hand", "head", "health", "heart", "heat", "height", "hell", "help", "henry", "hill", "history",
         "hole", "holiday", "home", "hope", "horse", "hospital", "hotel", "hour", "house", "household", "husband",
         "ice", "idea", "identity", "image", "impact", "importance", "impression", "improvement", "incident", "income",
         "increase", "independence", "index", "individual", "industry", "inflation", "influence", "information",
         "initiative", "injury", "inquiry", "instance", "institute", "institution", "insurance", "intelligence",
         "intention", "interest", "interpretation", "interview", "introduction", "investigation", "investment",
         "involvement", "iron", "island", "issue", "item", "jack", "jane", "japan", "job", "joe", "john", "jones",
         "journey", "judge", "justice", "key", "kind", "king", "kingdom", "kitchen", "knowledge", "labour", "lack",
         "lady", "lake", "land", "lane", "language", "law", "lead", "leader", "leadership", "league", "lee", "leg",
         "legislation", "length", "letter", "level", "lewis", "liability", "library", "licence", "life", "lifespan",
         "light", "limit", "line", "link", "list", "literature", "loan", "location", "look", "lord", "loss", "lot",
         "love", "lunch", "machine", "magazine", "maintenance", "major", "majority", "man", "management", "manager",
         "manchester", "manner", "map", "march", "mark", "market", "marriage", "martin", "mary", "mass", "master",
         "match", "material", "may", "meal", "measure", "meat", "member", "membership", "memory", "message", "metal",
         "method", "middle", "mike", "milk", "mill", "mind", "minister", "ministry", "minority", "minute", "mirror",
         "miss", "mistake", "model", "moment", "money", "month", "morning", "mother", "motion", "motor", "mountain",
         "mouth", "move", "movement", "mum", "murder", "museum", "music", "name", "nation", "nature", "neck", "need",
         "network", "new", "news", "newspaper", "night", "no.", "noise", "north", "northern", "nose", "note", "notice",
         "notion", "object", "occasion", "offence", "offer", "office", "officer", "oil", "operation", "opinion",
         "opportunity", "opposition", "option", "order", "organisation", "organization", "other", "outcome", "output",
         "owner", "oxford", "package", "page", "pain", "pair", "palace", "panel", "paper", "parent", "parish", "park",
         "parliament", "part", "partner", "partnership", "party", "passage", "past", "path", "patient", "pattern",
         "paul", "pay", "payment", "peace", "pension", "performance", "period", "person", "peter", "phase",
         "philosophy", "phone", "picture", "piece", "place", "plan", "plane", "plant", "plastic", "plate", "play",
         "player", "pleasure", "point", "police", "policy", "pollution", "pool", "population", "port", "position",
         "possibility", "post", "potential", "pound", "power", "pp.", "practice", "presence", "present", "president",
         "press", "pressure", "price", "prince", "principle", "priority", "prison", "problem", "procedure", "process",
         "product", "production", "professor", "profit", "program", "programme", "progress", "project", "property",
         "proportion", "proposal", "protection", "provision", "pub", "public", "publication", "purpose", "quality",
         "quarter", "queen", "question", "race", "radio", "rail", "railway", "rain", "range", "rate", "reaction",
         "reader", "reality", "reason", "recession", "recognition", "record", "recovery", "reduction", "ref",
         "reference", "reform", "regime", "region", "relation", "relationship", "release", "relief", "religion",
         "report", "representation", "republic", "reputation", "request", "research", "resistance", "resolution",
         "respect", "response", "responsibility", "rest", "restaurant", "result", "retirement", "return", "revenue",
         "review", "revolution", "right", "ring", "rise", "risk", "river", "road", "rock", "role", "roof", "room",
         "round", "route", "row", "rugby", "rule", "run", "safety", "sale", "sample", "scale", "scene", "scheme",
         "school", "science", "scope", "screen", "sea", "search", "season", "seat", "second", "secretary", "section",
         "sector", "security", "selection", "self", "sense", "sentence", "sequence", "series", "service", "session",
         "set", "settlement", "sex", "shape", "share", "sheet", "ship", "shock", "shop", "shoulder", "show", "side",
         "sight", "sign", "significance", "silence", "silver", "simon", "sir", "sister", "site", "situation", "size",
         "skill", "skin", "sky", "sleep", "smile", "smith", "snow", "society", "software", "soil", "solution", "son",
         "song", "sort", "sound", "source", "south", "space", "speaker", "specialist", "species", "speech", "speed",
         "spirit", "spokesman", "sport", "spot", "spring", "square", "staff", "stage", "standard", "star", "start",
         "state", "statement", "station", "status", "steel", "step", "stock", "stone", "store", "strategy", "street",
         "strength", "stress", "strike", "structure", "struggle", "student", "study", "stuff", "style", "subject",
         "success", "sugar", "sum", "summer", "sun", "supply", "support", "surface", "surprise", "survey", "system",
         "table", "talk", "tape", "target", "task", "tax", "taylor", "tea", "teacher", "team", "technique",
         "technology", "telephone", "television", "temperature", "term", "test", "text", "thatcher", "theatre", "theme",
         "theory", "thing", "threat", "time", "title", "tom", "tone", "tony", "top", "total", "touch", "tour", "town",
         "track", "trade", "tradition", "traffic", "train", "transfer", "transport", "travel", "treatment", "treaty",
         "tree", "trial", "trip", "trouble", "trust", "truth", "turn", "type", "uncle", "unemployment", "union", "unit",
         "university", "use", "user", "valley", "value", "van", "variety", "vehicle", "version", "victim", "victory",
         "video", "view", "village", "violence", "vision", "visit", "voice", "volume", "vote", "walk", "wall", "war",
         "waste", "water", "way", "wealth", "weather", "week", "weekend", "weight", "welfare", "west", "while", "white",
         "whole", "wife", "will", "wind", "window", "wine", "winter", "woman", "wood", "word", "work", "worker",
         "world", "writer", "year", "youth"]
imgs_per_term = 20


class CompressDataset(Dataset):
    def __init__(self, root_dir: Path, transform: Callable[[np.ndarray, ], Tensor], extension, train=False):
        super().__init__()
        self._root_dir = root_dir
        self._transform = transform
        self._format = extension

        if not self._root_dir.exists():
            self._download_data()

        selected_folders = {noun for idx, noun in enumerate(nouns) if bool(idx % 5) == train}
        self._img_list = [x for x in self._root_dir.rglob(f'*.{self._format}') if x.parent.stem in selected_folders]

    def __getitem__(self, index):
        path = self._img_list[index]

        img = cv2.imread(str(path))
        if img is None:
            raise FileNotFoundError(f"Couldn't load {path}")

        return self._transform(img)

    def __len__(self):
        return len(self._img_list)

    def _download_data(self):
        random.shuffle(nouns)
        for noun in tqdm(nouns, desc="Downloading images"):
            if (self._root_dir / noun).exists():
                continue
            r = google_images_download.googleimagesdownload()
            r.download({"keywords":         noun,
                        "limit":            imgs_per_term,
                        "usage_rights":     "labeled-for-reuse",
                        "size":             ">400*300",
                        "output_directory": str(self._root_dir.absolute()),
                        "format":           self._format,
                        "silent_mode":      True})

        for folder in tqdm(list(self._root_dir.iterdir()), desc="Combining similar folders"):
            if not folder.is_dir():
                continue
            stem = folder.stem
            pure = stem.split('-')[0].strip()
            if stem != pure:
                new_path = folder.parent / pure
                new_path.mkdir(exist_ok=True)
                for img in folder.glob('*'):
                    img.rename(new_path / img.name)
                folder.rmdir()

        for folder in tqdm(list(self._root_dir.iterdir()), desc="Removing duplicate images"):
            hashes = []
            for img_path in folder.rglob(f"*.{self._format}"):
                img = cv2.imread(str(img_path))
                if img is None:
                    img_path.unlink()
                    continue
                imghash = np.array(imagehash.phash(Image.fromarray(img)).hash.flatten())
                for h in hashes:
                    if np.sum(h ^ imghash) < 12:
                        img_path.unlink()
                        break
                else:
                    hashes.append(imghash)
