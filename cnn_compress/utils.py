import logging
import sys
from pathlib import Path
from typing import Optional, Callable, Tuple

import numpy as np
import torch
from PIL import Image
from torch import Tensor, nn
from torchvision.transforms import Normalize, Compose, ToTensor
from yacs.config import CfgNode as CN

from cnn_compress.config import transform_cfg, get_default_cfg, load_cfg as load_cfg_real
from cnn_compress.model_builder import create_combined_model


def cv2pil(img: np.ndarray) -> np.ndarray:
    return Image.fromarray(np.flip(img, axis=2))


def pil2cv(img) -> np.ndarray:
    return np.flip(np.array(img), axis=2)


def get_standard_transform(cfg: CN) -> Callable[[np.ndarray, ], Tensor]:
    return Compose([ToTensor(), Normalize(mean=cfg.DATA.PIXEL_MEANS, std=cfg.DATA.PIXEL_STDS)])


def get_standard_detransform(cfg: CN) -> Callable[[Tensor, ], np.ndarray]:
    return Compose([Normalize(mean=[0., 0., 0.], std=1 / np.array(cfg.DATA.PIXEL_STDS)),
                    Normalize(mean=-np.array(cfg.DATA.PIXEL_MEANS), std=[1., 1., 1.]),
                    lambda x: (x.permute(1, 2, 0).numpy() * 255).astype(np.int).clip(0, 255).astype(np.uint8)])


def create_logger(name: str = '', file_path: Optional[Path] = None, console_level=logging.INFO,
                  file_level=logging.DEBUG):
    formatter = logging.Formatter(fmt="[{levelname:^8s}] {asctime} {name:>20s}:{filename:>20s} {message}", style="{",
                                  datefmt="%Y-%m-%d %H:%M:%S")
    logger = logging.Logger(name)

    if file_path:
        file_handler = logging.FileHandler(file_path)
        file_handler.setFormatter(formatter)
        file_handler.setLevel(file_level)
        logger.addHandler(file_handler)

    console_handler = logging.StreamHandler(sys.stdout)
    console_handler.setFormatter(formatter)
    console_handler.setLevel(console_level)
    logger.addHandler(console_handler)

    return logger


def pad_image(img: np.ndarray) -> Tuple[np.ndarray, Tuple[int, int]]:
    height, width, num_channels = img.shape

    new_height = (np.ceil(height / 32) * 32).astype(np.int)
    new_width = (np.ceil(width / 32) * 32).astype(np.int)

    template = np.zeros((new_height, new_width, num_channels), dtype=img.dtype)
    template[:height, :width, :] = img
    return template, (height, width)


def load_cfg(cfg_path: Optional[Path] = None) -> CN:
    return transform_cfg(get_default_cfg()) if cfg_path is None else load_cfg_real(cfg_path)


def load_model(cfg: CN) -> nn.Module:
    model = create_combined_model(cfg.MODEL)

    ckpt = torch.load(Path(cfg.OUTPUT_DIR) / 'final.model')
    model.encoder.load_state_dict(ckpt['encoder'])
    model.decoder.load_state_dict(ckpt['decoder'])

    return model


def create_infer_transform(cfg: CN) -> Callable[[np.ndarray, ], Tensor]:
    return Compose([cv2pil, get_standard_transform(cfg)])


def create_infer_detransform(cfg: CN) -> Callable[[Tensor, ], np.ndarray]:
    return Compose([get_standard_detransform(cfg), pil2cv])
