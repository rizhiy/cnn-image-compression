import argparse
from pathlib import Path
from typing import Optional

import torch
from torch import nn
from torch.optim import SGD
from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter
from torchvision.transforms import RandomCrop, Compose, Resize
from tqdm import tqdm
from yacs.config import CfgNode as CN

from cnn_compress.config import get_default_cfg, load_cfg, transform_cfg
from cnn_compress.dataset import CompressDataset
from cnn_compress.model_builder import create_combined_model
from cnn_compress.utils import cv2pil, get_standard_transform, create_logger


def _create_optimiser(model: nn.Module, cfg: CN, lr: float):
    return SGD(model.parameters(), lr, cfg.TRAIN.MOMENTUM, weight_decay=cfg.TRAIN.WEIGHT_DECAY)


def train(cfg_path: Optional[Path] = None, num_workers=4, cpu=False) -> None:
    """
    Train a new model
    :param cfg_path: path to configuration
    :param num_workers: How many threads to use for loading data
    :param cpu: Whether to use CPU
    """
    GPU = not cpu
    cfg = transform_cfg(get_default_cfg()) if cfg_path is None else load_cfg(cfg_path)

    logger = create_logger(cfg.NAME + ' TRAIN', Path(cfg.OUTPUT_DIR) / 'train.log')

    writer = SummaryWriter(log_dir=cfg.OUTPUT_DIR, filename_suffix='.tb')

    transforms = [cv2pil]
    augs_cfg = cfg.TRAIN.AUGS
    if augs_cfg.RANDOM_CROP:
        transforms.append(RandomCrop(size=cfg.DATA.SIZE, pad_if_needed=True, padding_mode='symmetric'))
    transforms.append(Resize(size=(cfg.DATA.SIZE, cfg.DATA.SIZE)))
    transforms.append(get_standard_transform(cfg))
    transform = Compose(transforms)

    logger.info("Creating Dataset")
    dataset = CompressDataset(Path(cfg.DATA.ROOT_DIR), transform, cfg.DATA.FORMAT, train=True)
    logger.info(f"Dataset Created: {len(dataset)} images")

    dataloader = DataLoader(dataset, batch_size=cfg.TRAIN.BATCH_SIZE, shuffle=cfg.TRAIN.SHUFFLE,
                            num_workers=num_workers, pin_memory=True, drop_last=True)
    model = create_combined_model(cfg.MODEL)

    if GPU:
        model = nn.DataParallel(model.cuda())

    loader = iter(dataloader)

    criterion = nn.MSELoss()

    lr = cfg.TRAIN.LR
    optim = _create_optimiser(model, cfg, lr)

    for iter_idx in tqdm(range(cfg.TRAIN.NUM_ITERS), total=cfg.TRAIN.NUM_ITERS, smoothing=0.1):
        if iter_idx in cfg.TRAIN.STEPS:
            lr *= cfg.TRAIN.STEP_FACTOR
            optim = _create_optimiser(model, cfg, lr)
        try:
            batch = next(loader)
        except StopIteration:
            loader = iter(dataloader)
            batch = next(loader)
        if GPU:
            batch = batch.cuda()

        output = model(batch)

        loss = criterion(output, batch)
        optim.zero_grad()
        loss.backward()
        optim.step()

        writer.add_scalar('loss', float(loss), iter_idx)
        writer.add_scalar('lr', lr, iter_idx)
        logger.debug(f"ITER={iter_idx:05d}, LOSS={float(loss):5.3f}, LR={lr:.5f}")

    if isinstance(model, nn.DataParallel):
        model = model.module.cpu()
    torch.save({"encoder": model.encoder.state_dict(), "decoder": model.decoder.state_dict()},
               Path(cfg.OUTPUT_DIR) / 'final.model')
    writer.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser("Train the model")
    parser.add_argument('--cfg', type=Path, help="Path to config file")
    parser.add_argument('-nw', '--num-workers', type=int, default=4, help="How many workers to use")
    parser.add_argument('--cpu', action='store_true', help="Use CPU")
    args = parser.parse_args()

    train(args.cfg, args.num_workers, args.cpu)
