import os.path as osp
import random
from pathlib import Path

import numpy as np
import torch
from yacs.config import CfgNode as CN

_default_cfg_name = '__default__'
_default_root_output_dir = 'output'
_default_output_dir = osp.join(_default_root_output_dir, _default_cfg_name)

_cfg = CN()
_cfg.NAME = _default_cfg_name
_cfg.OUTPUT_DIR = _default_output_dir
_cfg.SEED = 42

_cfg.DATA = CN()
_cfg.DATA.ROOT_DIR = 'data'
# Possible values: jpg, gif, png, bmp, svg, webp, ico, raw
_cfg.DATA.FORMAT = 'jpg'
_cfg.DATA.SIZE = 224
_cfg.DATA.PIXEL_MEANS = [0.485, 0.456, 0.406]
_cfg.DATA.PIXEL_STDS = [0.229, 0.224, 0.225]

_cfg.MODEL = CN()
_cfg.MODEL.PRETRAINED = False
_cfg.MODEL.EXPANSION_BASE = 5

_cfg.TRAIN = CN()
_cfg.TRAIN.LR = 0.002
_cfg.TRAIN.MOMENTUM = 0.9
_cfg.TRAIN.WEIGHT_DECAY = 0.0001
_cfg.TRAIN.BATCH_SIZE = 128
_cfg.TRAIN.NUM_ITERS = 500
_cfg.TRAIN.STEPS = [350, 450]
_cfg.TRAIN.STEP_FACTOR = 0.1
_cfg.TRAIN.SHUFFLE = False
_cfg.TRAIN.AUGS = CN()
_cfg.TRAIN.AUGS.RANDOM_CROP = False

_cfg.TEST = CN()
_cfg.TEST.BATCH_SIZE = 0


def transform_cfg(cfg):
    cfg.OUTPUT_DIR = cfg.OUTPUT_DIR if cfg.OUTPUT_DIR != _default_output_dir \
        else osp.join(_default_root_output_dir, cfg.NAME)
    Path(cfg.OUTPUT_DIR).mkdir(exist_ok=True, parents=True)
    cfg.TEST.BATCH_SIZE = cfg.TEST.BATCH_SIZE or 2 * cfg.TRAIN.BATCH_SIZE
    return cfg


def load_cfg(cfg_path: Path):
    cfg = get_default_cfg()
    cfg.merge_from_file(cfg_path)

    cfg.NAME = cfg.NAME if cfg.NAME != _default_cfg_name else cfg_path.stem

    cfg = transform_cfg(cfg)
    update_seed(cfg.SEED)
    return cfg


def update_seed(seed: int):
    torch.manual_seed(seed)
    np.random.seed(seed)
    random.seed(seed)


def get_default_cfg():
    return _cfg.clone()
