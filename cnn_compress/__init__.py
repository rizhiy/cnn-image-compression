from .infer import compress, decompress
from .test import test as evaluate
from .train import train
