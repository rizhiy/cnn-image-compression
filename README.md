## Installation
I have tested this package with python3.7

To install run:
```bash
pip install .
```

## Usage
```python
from cnn_compress import compress, decompress, train, evaluate

img_path = "~/Pictures/example.png"

# Compress image
encoding_path = compress(img_path)

# Decompress image
decompress(encoding_path)

# Train new model
train()

# Test a model
evaluate()
```

## Changing Model
Model configuration is done through `cnn_compress.config`, see examples in "configs".

You can pass custom config to `train` and `evaluate`. 