from setuptools import setup, find_packages

setup(
    name="cnn_compress",
    version="0.0.0",
    author="Artem Vasenin",
    author_email="vasart169@gmail.com",
    packages=find_packages(),
    install_requires=['torch', 'torchvision', 'opencv-python', 'tqdm', 'yacs', 'tensorboard>=1.14', 'future', 'numpy',
                      'pillow', 'imagehash']
)
